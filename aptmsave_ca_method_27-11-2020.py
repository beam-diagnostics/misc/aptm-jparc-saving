#!/usr/bin/python3.5

import h5py
import os
import sys
import time
import epics
import signal

#####################################################
# Runtime definition
# _DEBUG == ESS or JPARC
#####################################################
_DEBUG = False
_USE_JPARC = True
_USE_EXTDRIVE = False

# Destination folder
if (_DEBUG == True):
	addr = os.getcwd() + '/'
	#addr = '/media/sf_vboxshared/'
else:
	if (_USE_EXTDRIVE == True):
		addr = '/mnt/external/aptm_datacollection-2020/'
	else:
		addr = '/opt/aptm_datacollection-2020/'

# Timestamping configs
TIMESTAMP = time.strftime("%H:%M:%S")
TIMESTAMP1 = time.strftime("%Y-%m-%d-%H:%M:%S")
FILENAME = addr + 'aptm-exp' + TIMESTAMP1 + '.hdf5'

# EPICS environment configuration
if (_DEBUG == True):
	os.environ['EPICS_CA_ADDR_LIST']='localhost'
else:
	#os.environ['EPICS_CA_ADDR_LIST']='10.41.16.16 10.41.17.16 10.41.18.16 10.41.16.17 10.41.17.17 10.41.18.17 10.41.19.17 10.41.70.1'
	epics_ca_pvs = '10.41.16.16 10.41.17.16 10.41.18.16 10.41.16.17 10.41.17.17 10.41.18.17 10.41.19.17 10.41.70.1 '
	epics_ca_pvs += '10.32.25.65 10.64.16.64 10.11.255.255 10.19.255.255 10.35.255.255 10.67.255.255 10.40.19.16 10.40.19.17 10.41.16.16 '
	epics_ca_pvs += '10.41.17.16 10.41.18.16 10.41.19.16 10.41.20.16 10.41.21.16 10.41.22.16 10.41.16.17 10.8.16.19 10.16.18.22 10.32.32.64 10.48.3.16 10.48.0.101 '
	epics_ca_pvs += '10.48.0.103 10.40.18.17 10.40.1.16 10.40.16.17 10.40.17.17 10.40.18.19 '
	epics_ca_pvs += '10.40.18.20 10.40.18.21 10.40.16.16 10.40.17.10 10.40.18.16 10.41.17.17 '
	epics_ca_pvs += '10.48.4.2 10.41.19.17 10.64.16.76 10.64.16.41 10.64.16.42 10.64.16.48 10.64.16.126 10.41.20.17 10.41.21.17 '
	epics_ca_pvs += '10.16.47.24 10.32.25.16 10.32.30.74 10.48.0.101 10.48.1.19 '
	epics_ca_pvs += '10.80.25.26 10.80.24.26 10.80.24.37 10.80.25.29 10.88.1.18 10.88.1.21'
	os.environ['EPICS_CA_ADDR_LIST']= epics_ca_pvs

# Global variables definitios
if (_DEBUG == True):
	prefix = 'LOC:'
else:
	prefix = 'APTM1:'

pvTriggered = False
pvTriggerName = 'APTM1-EVR1:EvtACnt-I'

# Creating HDF5 file
f=h5py.File(FILENAME,'w')
f.attrs['file_name']        = FILENAME
f.attrs['file_time']        = TIMESTAMP
f.attrs['HDF5_Version']     = h5py.version.hdf5_version

def signal_handler(sig, frame):
	global f
	print('Closing the HDF5 file ...')
	f.close()
	sys.exit(0)

# PV Callback function - just modify pvTriggered variable when it changes
def onChanges(pvname=None, value=None, char_value=None, **kw):
	global pvTriggered
	pvTriggered = True

# Main function
def main():
	global pvTriggered
	global f
	pvTriggered = False

	signal.signal(signal.SIGINT, signal_handler)

	# Trigger counter
	trgCounter = 0

	# ESS APTM PVs creation ######################################################################################
	ess_pv_list = []
	amc_list = ['AMC1:', 'AMC2:', 'AMC3:']
	amc_grid = ['AMC1:', 'AMC2:']
	ai_list  = ['AI1:','AI2:','AI3:','AI4:','AI5:','AI6:','AI7:','AI8:']
	ain_list = ['1:','2:','3:','4:','5:','6:','7:','8:']

	# Array Data PVs
	for amc_name in amc_list:
		for ai_n in ai_list:
			ess_pv_list.append(prefix + amc_name + ai_n + 'ArrayData')

	# Profile Data
	for amc_name in amc_grid:
		ess_pv_list.append(prefix + amc_name + 'Profile-RB')
		ess_pv_list.append(prefix + amc_name + 'ProfileFit-RB')

	# Fitting parameters
	for amc_name in amc_grid:
		ess_pv_list.append(prefix + amc_name + 'BackgroundR')
		ess_pv_list.append(prefix + amc_name + 'PeakAmplitudeR')
		ess_pv_list.append(prefix + amc_name + 'PeakMuR')
		ess_pv_list.append(prefix + amc_name + 'PeakAmplitudeR')
		ess_pv_list.append(prefix + amc_name + 'PeakSigmaR')
		ess_pv_list.append(prefix + amc_name + 'FNormR')
		ess_pv_list.append(prefix + amc_name + 'FitStatusR')

	# AMC Parameters
	for amc_name in amc_list:
		ess_pv_list.append(prefix + amc_name + 'TickValueR')
		ess_pv_list.append(prefix + amc_name + 'FSampR')
		ess_pv_list.append(prefix + amc_name + 'ArrayCounter_RBV')
		ess_pv_list.append(prefix + amc_name + 'ActualSamplesR')
		ess_pv_list.append(prefix + amc_name + 'TickValueR')
		for ai_n in ain_list:
			ess_pv_list.append(prefix + amc_name + ai_n + 'RangeR')

	# Motion PVs
	ess_pv_list.append('jparc:001-Err')
	ess_pv_list.append('jparc:001-ErrId')
	ess_pv_list.append('jparc:002-Err')
	ess_pv_list.append('jparc:002-ErrId')
	ess_pv_list.append('jparc:MCU-ErrMsg')
	ess_pv_list.append('jparc:ec0-s3-EL3314-AI1')
	ess_pv_list.append('jparc:ec0-s3-EL3314-AI2')
	ess_pv_list.append('jparc:MCU-ErrId')
	ess_pv_list.append('jparc:001.VAL')
	ess_pv_list.append('jparc:001.RBV')
	ess_pv_list.append('jparc:002.VAL')
	ess_pv_list.append('jparc:002.RBV')


	# Temperature blades
	ess_pv_list.append('APTM:TEMP1:Waveform')
	ess_pv_list.append('APTM:TEMP2:Waveform')


	# J-PARC PVs creation
	# PV for the grid upstream window
	# Observed beam profile at beam dump given as waveform of 32 channels for
	# each horizontal and vertical. Note: each wires has a space of 6 mm The
	# data is given by the value read out by ADC, which has a pedestal of 200
	# counts.
	# horizontal grid
	ess_pv_list.append('BT_SW:PMFIX0DH:VAL')
	# vertical grid
	ess_pv_list.append('BT_SW:PMFIX0DV:VAL')


	# Proton intensity observed at 3NBT	in unit of Tp (i.e. 1e12)
	ess_pv_list.append('BT_SW:SCTD:VAL_ATT')

	# Proton intensity observed by the current transformer at LINAC DTL given
	# the particles number per shots. (i.e. normally given in the range of
	# 1e12 to 1e14)
	#ess_pv_list.append('LI_DTL1:SCT01:MON:MLF_NPARTICLE')

	# Channel access connections
	# based on http://cars9.uchicago.edu/software/python/pyepics3/advanced.html

	# Create objects
	pvdata = {} # dictionary to hold chid
	pvdatadict = {} # dictionary to hold collected data

	print('Connecting to PVs...')

	for name in ess_pv_list:
		chid = epics.ca.create_channel(name, connect=False, auto_cb=False) # note 1
		pvdata[name] = (chid, None)

	# Connect to PVs
	for name, data in pvdata.items():
		epics.ca.connect_channel(data[0])
	epics.ca.poll()

	# PV used as a trigger for data collection
	acq_trigger = epics.PV(pvTriggerName)
	acq_trigger.add_callback(onChanges)

	# search for the position of the current pv in the list
	# exit when found and count is the name position in the list
	count=0
	for s in ess_pv_list:
		count =count + 1
		print(s)
		if ('LI_DTL1' in s):
			print(count)
			break


	while True:

		# Check if trigger occured
		if (pvTriggered == True):

			iter_start = time.perf_counter()

			# reset the trigger event to catch the next on
			pvTriggered = False

			# Get PV values
			for name, data in pvdata.items():
				epics.ca.get(data[0], wait=False)  # note 2

			epics.ca.poll()

			for name, data in pvdata.items():
				val = epics.ca.get_complete(data[0])
				pvdatadict[name] = val

			getpv_end = time.perf_counter()


			#f=h5py.File(FILENAME,'a')
			my_grp=f.create_group(str(trgCounter))

			for name, data in pvdata.items():
				my_grp.create_dataset(name, data=pvdatadict[name])
				#print(name + ' = ' + str(pvdatadict[name]))

			my_grp.create_dataset('TIMESTAMP', data=acq_trigger.timestamp)

			now = time.perf_counter()
			getpv_time = getpv_end - iter_start
			writepv_time = now - getpv_end

			print(time.strftime("[%Y/%m/%d %H:%M:%S]") + ' .... Saving collected data | ' + '{:0.4f} | {:0.4f}'.format(getpv_time, writepv_time))
			trgCounter = trgCounter + 1
			#f.close()

			# print the value from the pv reporting the current
			#Nproton = val(count)
			#print(" N protons in Linac = " + str(Nproton) + " x 1e12")


		time.sleep(0.25)


if __name__ == '__main__':
	main()
